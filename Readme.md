# Muchi
An automated prank machine

## Requirements:
  1. Go 1.6+
  2. github.com/faiface/beep

## Build for Windows:
```make buildwin```

## Build for Linux:
```make build```

## Running:
```./muchi -delay=30m -audioDir="sounds/best"```

You can also display a help messeage via
```./muchi -h```