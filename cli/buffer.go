package main

import (
	"bytes"
	"io/ioutil"
)

type ClosingBuffer struct {
	*bytes.Buffer
}

func (cb *ClosingBuffer) Close() (err error) {
	return
}

func (cb *ClosingBuffer) Copy() *ClosingBuffer {
	return &ClosingBuffer{bytes.NewBuffer(cb.Bytes())}
}

func readFileClosingBuffer(filename string) (*ClosingBuffer, error) {
	fileBytes, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}

	return &ClosingBuffer{bytes.NewBuffer(fileBytes)}, nil
}
