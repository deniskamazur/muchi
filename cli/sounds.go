package main

import (
	"io"
	"time"

	"github.com/faiface/beep"
	"github.com/faiface/beep/mp3"
	"github.com/faiface/beep/speaker"
)

func playSound(f io.ReadCloser) (chan struct{}, error) {
	done := make(chan struct{}, 1)

	s, format, err := mp3.Decode(f)
	if err != nil {
		close(done)
		return done, err
	}

	speaker.Init(format.SampleRate, format.SampleRate.N(time.Second/10))
	speaker.Play(beep.Seq(s, beep.Callback(func() {
		time.Sleep(time.Millisecond * 100)
		done <- struct{}{}
	})))

	return done, nil
}
