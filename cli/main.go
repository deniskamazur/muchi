package main

import (
	"flag"
	"io/ioutil"
	"log"
	"path/filepath"
	"time"
)

var (
	delay       *time.Duration
	audioDir    *string
	apiEndpoint *string
)

func main() {
	// parse flags
	delay = flag.Duration("delay", time.Second, "delay between each gachimuchi sound")
	audioDir = flag.String("audioDir", "sounds/best/", "directory with audio files to be played")
	apiEndpoint = flag.String("apiEndpoint", "http:/localhost:8000/", "much api endpoint")

	flag.Parse()

	// list all files in provided audio directory
	fileInfos, err := ioutil.ReadDir(*audioDir)
	if err != nil {
		panic(err)
	}

	// load all files in directory
	var audioBuffers map[string]*ClosingBuffer
	for _, fileInfo := range fileInfos {
		buffer, err := readFileClosingBuffer(filepath.Join(*audioDir, fileInfo.Name()))
		if err != nil {
			log.Printf("error occured while reading file %s: %e", fileInfo.Name(), err)
		} else {
			audioBuffers[fileInfo.Name()] = buffer
		}
	}

	// check any files were loaded
	if len(audioBuffers) == 0 {
		panic("couldn't find playable auido files :(")
	}

}
