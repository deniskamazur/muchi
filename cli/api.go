package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/pkg/errors"
)

func addDevice(apiEndpoint string, audioFiles []string) (string, error) {
	request := struct {
		Audio []string `json:"audio"`
	}{
		audioFiles,
	}

	requestBytes, err := json.Marshal(request)
	if err != nil {
		return "", err
	}

	response, err := http.Post(apiEndpoint+"devices/", "json", bytes.NewReader(requestBytes))
	if err != nil {
		return "", err
	}

	if response.StatusCode != http.StatusOK {
		return "", errors.New("unsopported status code")
	}

	var data struct {
		ID string `json:"id"`
	}

	if err := json.NewDecoder(response.Body).Decode(&data); err != nil {
		return "", err
	}

	return data.ID, nil
}

func popQueue(apiEndpoint, deviceID string) (string, error) {
	response, err := http.Get(apiEndpoint + "queues/" + deviceID)
	if err != nil {
		return "", err
	}

	if response.StatusCode != http.StatusOK {
		return "", errors.New(fmt.Sprintf("bad respose code: %d", response.StatusCode))
	}

	var data struct {
		Audio string `json:"audio"`
	}

	if err := json.NewDecoder(response.Body).Decode(&data); err != nil {
		return "", err
	}

	return data.Audio, nil
}
