package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"

	"gopkg.in/mgo.v2/bson"

	"github.com/gorilla/context"
	mgo "gopkg.in/mgo.v2"
)

func addDeviceHandler(w http.ResponseWriter, r *http.Request) {
	var request struct {
		Audio []string `json:"audio,omitempty"`
	}

	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	db := context.Get(r, "db").(*mgo.Database)

	device := *NewDevice()

	if err := db.C("devices").Insert(device); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	responseBytes, err := json.Marshal(device)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	fmt.Fprint(w, string(responseBytes))
}

func getDevicesHandler(w http.ResponseWriter, r *http.Request) {
	db := context.Get(r, "db").(*mgo.Database)

	var devices []Device
	if err := db.C("devices").Find(bson.M{}).All(&devices); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	responseBytes, err := json.Marshal(devices)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	fmt.Fprintf(w, string(responseBytes))
}

func devicesHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		getDevicesHandler(w, r)
	case http.MethodPost:
		addDeviceHandler(w, r)
	}
}

func updateAudioHandler(w http.ResponseWriter, r *http.Request) {
	var request struct {
		Auido string `json:"audio"`
	}

	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	vars := mux.Vars(r)
	device, ok := vars["device"]
	if !ok {
		http.Error(w, "device id not specified", http.StatusBadRequest)
		return
	}

	db := context.Get(r, "db").(*mgo.Database)

	if err := db.C("devices").Update(bson.M{"_id": device}, bson.M{"$addToSet": bson.M{"audio": request.Auido}}); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func getAudioHandler(w http.ResponseWriter, r *http.Request) {
	deviceID, ok := mux.Vars(r)["device"]
	if !ok {
		http.Error(w, "device id not specified", http.StatusBadRequest)
		return
	}

	db := context.Get(r, "db").(*mgo.Database)

	var device Device
	if err := db.C("devices").Find(bson.M{"_id": deviceID}).One(&device); err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	var response struct {
		Audio []string `json:"audio"`
	}

	response.Audio = device.Audio

	responseBytes, err := json.Marshal(response)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	fmt.Fprint(w, string(responseBytes))
}

func pushAudioQueue(w http.ResponseWriter, r *http.Request) {
	deviceID, ok := mux.Vars(r)["device"]
	if !ok {
		http.Error(w, "device id not specified", http.StatusBadRequest)
		return
	}

	log.Printf("found deviceID: %s", deviceID)

	var request struct {
		Audio string `json:"audio"`
	}

	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	db := context.Get(r, "db").(*mgo.Database)

	if err := db.C("devices").UpdateId(bson.ObjectIdHex(deviceID), bson.M{"$push": bson.M{"queue": request.Audio}}); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func popAudioQueueHandler(w http.ResponseWriter, r *http.Request) {
	// get deviceID from url string
	deviceID, ok := mux.Vars(r)["device"]
	if !ok {
		http.Error(w, "device id not cpecified", http.StatusBadRequest)
	}

	db := context.Get(r, "db").(*mgo.Database)

	// find device in database
	var device Device
	if err := db.C("devices").FindId(bson.ObjectIdHex(deviceID)).One(&device); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// check if device has any audios in queue
	if len(device.AudioQueue) == 0 {
		http.Error(w, "audio queue empty", http.StatusExpectationFailed)
		return
	}

	// create response
	var response struct {
		Audio string `json:"audio"`
	}
	response.Audio = device.AudioQueue[0]
	responseBytes, err := json.Marshal(response)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	fmt.Fprint(w, string(responseBytes))

	// delete oldest audio from "queue"
	if err := db.C("devices").UpdateId(bson.ObjectIdHex(deviceID), bson.M{"queue": device.AudioQueue[1:]}); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func queueHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		popAudioQueueHandler(w, r)
	case http.MethodPost:
		pushAudioQueue(w, r)
	}
}
