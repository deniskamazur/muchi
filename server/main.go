package main

import (
	"flag"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/handlers"
	"github.com/gorilla/sessions"

	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2"
)

var port *string

var (
	headersOk = handlers.AllowedHeaders([]string{"X-Requested-With"})
	originsOk = handlers.AllowedOrigins([]string{os.Getenv("ORIGIN_ALLOWED")})
	methodsOk = handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "OPTIONS"})

	cookieStore *sessions.CookieStore
)

func main() {
	port = flag.String("port", ":8000", "server http port")

	// dial and connect to database
	sess, err := mgo.Dial("localhost")
	if err != nil {
		panic(err)
	}
	db := sess.DB("muchi")

	router := mux.NewRouter()

	router.Handle("/devices/", Adapt(http.HandlerFunc(devicesHandler), withDB(db))).Methods("GET", "POST")
	router.Handle("/queues/{device}", Adapt(http.HandlerFunc(queueHandler), withDB(db))).Methods("GET", "POST")

	log.Printf("server running on port %s\n", *port)
	http.ListenAndServe(*port, handlers.CORS(originsOk, headersOk, methodsOk)(router))
}
