package main

import (
	"gopkg.in/mgo.v2/bson"
)

// Device stores device data
type Device struct {
	ID bson.ObjectId `json:"id" bson:"_id"`

	Audio      []string `json:"audio" bson:"audio"`
	AudioQueue []string `json:"queue" bson:"queue"`
}

// NewDevice createas a new instance of Device
func NewDevice() *Device {
	device := new(Device)
	device.ID = bson.NewObjectId()

	return device
}

// AccessKey stores access key data
type AccessKey struct {
	Value string `json:"value" bson:"value"`
}
