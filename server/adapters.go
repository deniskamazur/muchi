package main

import (
	"net/http"

	"github.com/gorilla/context"
	"github.com/gorilla/sessions"
	mgo "gopkg.in/mgo.v2"
)

// Adapter is an http adapter
type Adapter func(http.Handler) http.Handler

// Adapt adapts a http handler
func Adapt(h http.Handler, adapters ...Adapter) http.Handler {
	for _, adapter := range adapters {
		h = adapter(h)
	}
	return h
}

func withDB(db *mgo.Database) Adapter {
	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			context.Set(r, "db", db)
			h.ServeHTTP(w, r)
		})
	}
}

func withAccess(store sessions.CookieStore, onDenie http.HandlerFunc) Adapter {
	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if session, err := store.Get(r, "rmonitor"); err == nil {
				if v, ok := session.Values["access"]; ok && v.(bool) == true {
					h.ServeHTTP(w, r)
					return
				}
				onDenie.ServeHTTP(w, r)
			} else {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
		})
	}
}

func withSession(store sessions.CookieStore) Adapter {
	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if session, err := store.Get(r, "rmonitor"); err == nil {
				context.Set(r, "session", session)
				h.ServeHTTP(w, r)
			} else {
				http.Error(w, err.Error(), http.StatusInternalServerError)
			}
		})
	}
}
