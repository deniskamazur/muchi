# Go parameters
GOCMD=go
GOBUILD=$(GOCMD) build
GOCLEAN=$(GOCMD) clean
GOTEST=$(GOCMD) test
GOGET=$(GOCMD) get
BIN=muchi
WINBIN=muchi.exe

all: clean build run

clean:
	$(GOCLEAN)
	rm -f $(BIN)
	rm -f $(WINBIN)

build: clean
	$(GOBUILD) -o $(BIN) .

run: build
	./$(BIN)

buildwin: clean 
	GOOS=windows GOARCH=386 go build -o $(WINBIN) .
